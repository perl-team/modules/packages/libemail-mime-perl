libemail-mime-perl (1.954-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.954.
    + Fix excessive memory use issue, which can cause denial of service when
      parsing multipart MIME messages (CVE-2024-4140) (Closes: #960062)
  * Declare compliance with Debian policy 4.7.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 03 May 2024 21:32:44 +0200

libemail-mime-perl (1.953-1) unstable; urgency=medium

  * Import upstream version 1.953.
  * Update upstream email address.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Jan 2023 00:15:08 +0100

libemail-mime-perl (1.952-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libemail-simple-perl.
    + libemail-mime-perl: Drop versioned constraint on libemail-simple-perl in
      Depends.

  [ gregor herrmann ]
  * Import upstream version 1.952.
  * Update email address for Upstream-Contact.
  * Update years of packaging copyright.
  * Drop pod-whatis.patch, fixed upstream.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Dec 2021 18:31:42 +0100

libemail-mime-perl (1.949-1) unstable; urgency=medium

  * Team upload

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Salvatore Bonaccorso ]
  * Allow "0" as boundary value (Closes: #944887)

  [ Joenio Marques da Costa ]
  * Import upstream version 1.949
  * Set Rules-Requires-Root: no
  * d/control: depends on libemail-mime-contenttype-perl => 1.023
  * d/control: remove version from libemail-mime-encodings-perl
  * Allow-0-as-boundary-value patch accepted on upstream
  * declare compliance with Debian Policy 4.5.0
  * update debhelper compat to 13
  * Add myself to d/copyright

  [ gregor herrmann ]
  * Annotate test-only build dependencies with <!nocheck>.

 -- Joenio Marques da Costa <joenio@joenio.me>  Sat, 07 Nov 2020 18:28:50 +0100

libemail-mime-perl (1.946-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * New upstream version 1.946
  * replace (build) dependency on libemail-address-perl with
    libemail-address-xs-perl
  * bump the version of the libemail-mime-contenttype-perl (build)
    dependency
  * bump the version of the libemail-simple-perl (build) dependency
  * add (build) dependency on libmodule-runtime-perl
  * update debian/copyright

  [ gregor herrmann ]
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.1.1.
  * Drop ancient alternative build dependency on Test::More.
  * Add patch to fix POD in new module.

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Nov 2017 20:01:13 +0100

libemail-mime-perl (1.937-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 1.937

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 01 Feb 2016 23:27:17 -0200

libemail-mime-perl (1.936-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.936

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Wed, 16 Sep 2015 10:44:10 -0300

libemail-mime-perl (1.934-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.934
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 21 Aug 2015 14:30:16 -0300

libemail-mime-perl (1.926-1) unstable; urgency=medium

  * New upstream release.
    Fixes "use of uninitialized values in Encode.pm and Header.pm"
    (Closes: #733317)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Feb 2014 17:26:45 +0100

libemail-mime-perl (1.925-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch: drop unneeded uversionmangle.
  * Drop build dependency on libcapture-tiny-perl.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 11 Dec 2013 21:42:01 +0100

libemail-mime-perl (1.924-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Closes: #723933
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 3.9.4.
  * Update list of upstream copyright holders.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 20:06:40 +0200

libemail-mime-perl (1.911-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 1.911
    + require a newer Email::Simple and cope with its improved line ending
      tweaks (Closes: #682629).
  * Bump dependency for Email::Simple.
    Bump (Build-)Depends(-Indep) on libemail-simple-perl to (>= 2.102).
  * Update format for debian/copyright file.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
  * Bump Standards-Version to 3.9.3
  * Drop unneeded Breaks and Replaces.
    Drop the unneeded Breaks and Replaces on libemail-mime-modifier-perl
    (<< 1.900-1) and libemail-mime-creator-perl (<< 1.900-1) as these are
    already superseded in Squeeze.
  * Bump Debhelper compatibility level to 8.
    Adjust Build-Depends on debhelper to (>= 8).

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 24 Jul 2012 20:00:11 +0200

libemail-mime-perl (1.910-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 15 Sep 2011 21:55:35 +0200

libemail-mime-perl (1.909-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/control: Convert Vcs-* fields to Git.
  * Bump Standards-Version to 3.9.2 (no changes).

  [ gregor herrmann ]
  * Remove transitional dummy packages.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 10 Sep 2011 21:00:07 +0200

libemail-mime-perl (1.907-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Refresh copyright information

  [ gregor herrmann ]
  * debian/control: remove version from libemail-simple-perl (build)
    dependency, already satisfied in lenny.

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 21 Feb 2011 09:38:51 -0500

libemail-mime-perl (1.906-1) unstable; urgency=low

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Russ Allbery ]
  * Remove myself from Uploaders.

  [ Ansgar Burchardt ]
  * New upstream release.
  * Add build-dep on perl (>= 5.10.1) | libtest-simple-perl (>= 0.88).
  * debian/copyright: Formatting changes; refer to "Debian systems" instead of
    "Debian GNU/Linux systems"; refer to /usr/share/common-licenses/GPL-1.
  * Use source format 3.0 (quilt).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 07 Nov 2010 13:44:49 +0100

libemail-mime-perl (1.903-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * debian/control: replace (build) dependency on libemail-simple-
    creator-perl with "libemail-simple-perl (>= 2.100) | libemail-
    simple-creator-perl".

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 23 Dec 2009 08:53:56 -0500

libemail-mime-perl (1.902-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 12 Nov 2009 19:35:57 -0500

libemail-mime-perl (1.901-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Update dependencies
    + Bump requirement on libemail-mime-encodings-perl (>= 1.313)
      per upstream
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Add myself to Uploaders and Copyright
  * Use new short debhelper rules format
  * Refresh copyright information
  * Provide Email::MIME::Modifier (libemail-mime-modifier-perl) and
    Email::MIME::Creator (libemail-mime-creator-perl) as a transition
    for the merge; once reverse dependencies are fixed, the virtual
    packages they provide can be removed
  * Add transitional dummy packages. These should be removed after
    the release of squeeze.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 08 Nov 2009 15:36:50 -0500

libemail-mime-perl (1.863-1) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * New upstream release.
  * Latest upstream release version is 1.861_01, using 1.861.01 instead.
  * Upgraded to debhelper 7.

  [ Brian Cassidy ]
  * New upstream release
  * debian/control: added myself to uploaders
  * debian/copyright: pull copyright text from lib/Email/MIME.pm

  [ gregor herrmann ]
  * debian/watch: use dist-based URL and add uversionmangle to deal with _ in
    upstream versions.
  * debian/control:
    - bump versioned (build) dependency on libemail-simple-perl to >= 2.004
      according to upstream changes
    - set Standards-Version to 3.8.0 (no changes)
    - switch Vcs-Browser field to ViewSVN
    - add ${misc:Depends} to Depends: field
  * debian/copyright:
    - switch to new format
    - bump years of packaging copyright

 -- Brian Cassidy <brian.cassidy@gmail.com>  Fri, 30 Jan 2009 09:57:11 -0400

libemail-mime-perl (1.861-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists (Closes: #467836).

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to debhelper 6

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 14 Jan 2008 10:48:47 -0430

libemail-mime-perl (1.861-2) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Fixed Maintainer field in control file.
  * Fixed copyright and control file with a better URL.

  [ Russ Allbery ]
  * Update standards version to 3.7.3 (no changes required).
  * Fix debhelper build dependency to match debian/compat.
  * Build-depend on libtest-pod-perl to enable POD tests.

 -- Russ Allbery <rra@debian.org>  Wed, 26 Dec 2007 14:33:16 -0800

libemail-mime-perl (1.861-1) unstable; urgency=high

  [ Ernesto Hernández-Novich (USB) ]
  * New upstream release (Closes: #420859).
  * Moved package into Debian Pkg Perl Project SVN.
  * Fixed copyright file with a better URL.
  * Cleanup debian/rules.
  * Fixed watch file.

  [ Damyan Ivanov ]
  * debian/watch: version=3; stop capturing file extensions; escape periods
  * debian/rules: fix stamp/non-stamp dependencies. Fixes parallel build
  * debian/control:
    + wrap long fields
    + drop leading space in short description
    + add muself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Thu, 15 Nov 2007 15:10:40 +0200

libemail-mime-perl (1.860-1) unstable; urgency=high

  * New upstream release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Wed, 18 Jul 2007 15:32:18 -0400

libemail-mime-perl (1.859-1) unstable; urgency=high

  * New upstream release (Closes: #420859, #422479).

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 02 Jul 2007 11:28:29 -0400

libemail-mime-perl (1.857-1) unstable; urgency=high

  * New upstream release (Closes: #403088)

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Thu, 14 Dec 2006 15:53:00 -0400

libemail-mime-perl (1.851-1) unstable; urgency=low

  * New upstream Release.
  * Fixed watch file.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Fri, 01 Sep 2006 10:22:34 -0400

libemail-mime-perl (1.82-1) unstable; urgency=low

  * Initial Release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 05 Dec 2005 11:08:09 -0400
